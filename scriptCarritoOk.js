
    let articles = [{
        "id": 1,
        "image": "./img/img1.jpg",
        "name": "chomba gris",
        "price": 23000,
        "quantity": 1,
        "description": "chomba de invierno talla l"
    },{
        "id": 2,
        "image": "./img/img2.jpg",
        "name": "falda azul",
        "price": 23000,
        "quantity": 1,
        "description": "chomba de invierno talla l"
    },{
        "id": 3,
        "image": "./img/img3.jpg",
        "name": "vestido floreado",
        "price": 23000,
        "quantity": 1,
        "description": "chomba de invierno talla l"
    },{
        "id": 4,
        "image": "./img/img4.jpg",
        "name": "chaqueton beige",
        "price": 23000,
        "quantity": 1,
        "description": "chomba de invierno talla l"
    },{       
        "id": 5,
        "image": "./img/img5.jpg",
        "name": "parka azul",
        "price": 53000,
        "quantity": 1,
        "description": "chomba de invierno talla l"
    },
];
    let carrito = {

        "articles": [],
        "total": 0
    }

    const bringListArticles = () => {
        return articles;
    };

    const bringArticlesById = (id) => {
        let articuloRetorno = null;
        articles.forEach(article => { 
            if (article.id == id) {
                articuloRetorno = article;
            } else {                
            }
        });
        return articuloRetorno;
    }
    //console.log(bringArticlesById(5));

    const addArticleCar = (id) => {
        carrito.articles.push(bringArticlesById(id));
    }

    const deleteArticleCar = (id) => {
        carrito.articles.splice(findPosition(id), 1);
    }
    const findPosition = (id) => {
        let indice = -1;
        carrito.articles.forEach((article, i)=>{
            if(article.id == id){
                indice = i;
            }
        });
        return indice;
    }

    const calculateTotalShop = () => {
        let sumTotal = 0;
        carrito.articles.forEach((article)=>{
                sumTotal += article.price*article.quantity;     
        });
        return sumTotal;
    }

    const generateHTMLArticles = () => {
        contenido = document.getElementById("contenido");
        let html = "";
        articles.forEach(article => {
            html += "<div>";
            html += "<img src='"+ article.image +"' onclick = 'generateModalArticle()'/>";
            html += article.name;
            html += "<div class= 'hide'>" + article.description + "</div>"
            html += "<div class= 'hide'>" + article.price + "</div>"
            html += "</div>";
        });
        contenido.innerHTML += html;
    };

    const generateModalArticle = (id) => {
        let modalArticle = document.getElementById("modalArticle");
        let article = bringArticlesById(id);
        let html = "";
        articles.forEach(article => {
            html += "<div>";
            html += "<img src='"+ article.image +"' onclick = 'generateModalArticle()'/>";
            html += article.name;
            html += "<div>" + article.description + "</div>"
            html += "<div>" + article.price + "</div>"
            html += "<button onclick = 'addArticleCar(id)'>Agregar</button>"
            html += "</div>";
        });
        modalArticle.innerHTML += html;
    };

    const generateCar = () => {
        let carritoHTML = document.getElementById("carrito");
        let html = "";
        html += "<table>"
        html += "<tr>";
        html += "<td>" + Nombre + "<td>";
        html += "<td>" + Cantidad + "</td>";
        html += "<td>" + Precio + "</td>";
        html += "<td>" + Total + "</td>";
        html += "<tr>";
        carrito.articles.forEach(article => {
            html += "<tr>";
            html += "<td>" + article.name + "<td>";
            html += "<td>" + article.quantity + "</td>";
            html += "<td>" + article.price + "</td>";
            html += "<td>" + article.quantity * article.price + "</td>";
            html += "<tr>";
        });
    };

    console.log(findPosition(3));
    console.log(carrito);
    addArticleCar(3);
    addArticleCar(4);
    addArticleCar(1);
    addArticleCar(5);
    console.log(carrito);
    deleteArticleCar(5);
    console.log(carrito);
    console.log(calculateTotalShop());
